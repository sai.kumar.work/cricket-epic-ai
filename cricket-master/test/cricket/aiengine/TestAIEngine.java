package cricket.aiengine;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cricket.graphstats.DrawScoringCharts;
import cricket.model.Batsman;
import cricket.model.BatsmanStats;
import cricket.model.Bowler;
import cricket.model.BowlerStats;
import cricket.model.MatchInfo;
import cricket.model.MatchState;
import cricket.model.ResultHistory;
import cricket.model.Team;
import cricket.utils.Pair;
import cricket.utils.Utils;
import cricket.model.Fielder;
import cricket.model.WicketKeeper;

public class TestAIEngine {

    private static Team createIndianTeam() {
        Team ret = new Team("India");
        
        //4th parameter
        //signature shot: 1-7
        //1: cut shot
        //2: pull shot
        //3: cover drive
        //4: helicopter-shot
        //5: sweep shot
        //6: inside-out
        //7: default
        //5th parameter : speed(0-10) : 5: moderate, 7: fast, 9: very fast
        //6th parameter : categorized ( who can play against different types of bowlers for instace, kohli is good with fast bowlers compared to spin
        // so we have created three categories)
        // category 1 --> (hitter, fast)
        // category 2 --> (hitter,spin)
        // category 3 --> (spin)
        // category 4 --> (fast)
        // category 5 --> perfectly balanced
        // category 6 --> they are bowlers
        ret.addBatsman(new Batsman("S. Dhawan", 8, 8, 7,1,7,4));
        ret.addBatsman(new Batsman("R. Sharma", 8, 8, 8,2,6,2));
        ret.addBatsman(new Batsman("V. Kohli", 9, 8, 8, 3,9,5));
        ret.addBatsman(new Batsman("M. Dhoni", 9, 8, 9,4,9,2));
        ret.addBatsman(new Batsman("Y. Singh", 8, 8, 7, 5,7,1));
        ret.addBatsman(new Batsman("S. Raina", 8, 6, 7,6,9,4));
        ret.addBatsman(new Batsman("R. Jadeja", 7, 7, 6,7,8,6));
        ret.addBatsman(new Batsman("B. Kumar", 6, 6, 6,7,5,6));
        ret.addBatsman(new Batsman("A. Nehra", 7, 6, 6,7,5,5));
        ret.addBatsman(new Batsman("R. Ashwin", 7, 7, 6, 7,6,6));
        ret.addBatsman(new Batsman("I. Sharma", 6, 6, 6,7,5,6));
        
        // 1 = fast bowler
        // 2 = spinner
        ret.addBowler(new Bowler("S. Raina", 8, 7, 3, 2));
        ret.addBowler(new Bowler("R. Jadeja", 8, 6, 3, 2));
        ret.addBowler(new Bowler("B. Kumar", 8, 7, 7, 1));
        ret.addBowler(new Bowler("A. Nehra", 8, 8, 8, 1));
        ret.addBowler(new Bowler("R. Ashwin", 8, 6, 3, 2));
        ret.addBowler(new Bowler("I. Sharma", 8, 6, 8, 1));
        
        // fielders 
        // 2nd parameter: fielder"0" or wicket-keeper "1"
        // 4th parameter: accuracy
        ret.addFielder(new Fielder("D. Warner", 7, 0,8));
        ret.addFielder(new Fielder("M. Clarke", 8, 0,9));
        ret.addFielder(new Fielder("S. Smith", 9, 0,9));
        ret.addFielder(new Fielder("S. Marsh", 9, 0,7));
        ret.addFielder(new Fielder("G. Bailey",9, 0,7));
        ret.addFielder(new Fielder("M. Starc", 7, 0,7));
        ret.addFielder(new Fielder("M. Johnson", 7, 0,6));
        ret.addFielder(new Fielder("M. Marsh", 7, 0,6));
        ret.addFielder(new Fielder("X. Doherty", 7, 0,6));
        ret.addFielder(new Fielder("P. Cummings", 9, 0,6));
        
        //wicket-keeper
        // 3rd parameter defines the precision
        ret.addWicketKeeper(new WicketKeeper("A. Finch", 8));
        return ret;
    }
    
    private static Team createAustralianTeam() {
        Team ret = new Team("Australia");
        ret.addBatsman(new Batsman("A. Finch", 8, 8, 8,1,8,1));
        ret.addBatsman(new Batsman("D. Warner", 8, 8, 8,2,7,1));
        ret.addBatsman(new Batsman("M. Clarke", 8, 9, 8,1,8,5));
        ret.addBatsman(new Batsman("S. Smith", 9, 8, 8,4,9,5));
        ret.addBatsman(new Batsman("S. Marsh", 8, 8, 9,7,8,4));
        ret.addBatsman(new Batsman("G. Bailey", 9,8,8,7,7,4));
        ret.addBatsman(new Batsman("M. Starc", 6, 6, 6,7,6,6));
        ret.addBatsman(new Batsman("M. Johnson", 6, 5, 7,7,5,6));
        ret.addBatsman(new Batsman("M. Marsh", 7, 7, 7,7,5,6));
        ret.addBatsman(new Batsman("X. Doherty", 6, 6, 6,7,5,6));
        ret.addBatsman(new Batsman("P. Cummings", 5, 5, 6,7,5,6));
        
        ret.addBowler(new Bowler("S. Smith", 8, 7, 3,2));
        ret.addBowler(new Bowler("M. Starc", 8, 6, 8, 2));
        ret.addBowler(new Bowler("M. Johnson", 8, 7, 7, 1));
        ret.addBowler(new Bowler("X. Doherty", 8, 8, 3, 2));
        ret.addBowler(new Bowler("P. Cummings", 8, 6, 6, 1));
        ret.addBowler(new Bowler("M. Marsh", 8, 6, 3, 1));
        
        // fielders 
        // 2nd parameter: fielder"0" or wicket-keeper "1"
        ret.addFielder(new Fielder("S. Dhawan", 8, 0,7));
        ret.addFielder(new Fielder("R. Sharma", 7, 0,7));
        ret.addFielder(new Fielder("V. Kohli", 9, 0,9));
        ret.addFielder(new Fielder("Y. Singh", 9, 0,9));
        ret.addFielder(new Fielder("S. Raina", 9, 0,9));
        ret.addFielder(new Fielder("R. Jadeja", 9, 0,9));
        ret.addFielder(new Fielder("B. Kumar", 7, 0,6));
        ret.addFielder(new Fielder("A. Nehra", 7, 0,6));
        ret.addFielder(new Fielder("R. Ashwin", 7, 0,6));
        ret.addFielder(new Fielder("I. Sharma", 7, 0,6));
        
        //wicket keeper
        ret.addWicketKeeper(new WicketKeeper("M. Dhoni", 9));
        return ret;
    }

	
	private static AIEngine simulateInnings(int maxOvers, int targetScore, boolean printResults) {
		MatchInfo mi = Utils.createMatchInfo(maxOvers, createIndianTeam(), createAustralianTeam());
		// decide who bats first
		int battingTeamIndex = 1;
		AIEngine engine = Utils.createAIEngine(mi, battingTeamIndex, false, targetScore, 1, 1);
		engine.simulateInnings();
		if (printResults) {
			engine.printInningsDetail();
			engine.printInningsSummary();
		}
		return engine;
	}
	
	private static void printAverageBatsmanStats(Batsman batsman, List<BatsmanStats> stats) {
		double played = 0, runs = 0, sr = 0, out = 0;
		for (BatsmanStats batStats : stats) {
			if (batStats.getBallsFaced()>0) {
				runs += batStats.getRunsScored();
				sr += batStats.getStrikeRate();
				played++;
				if (batStats.gotOut()) {
					out++;
				}
			}
		}
		System.out.println("Batsman: "+batsman.getName()+"\tPlayed: "+played
				+"\tAvg-Runs: "+(runs/out)+"\tAvg-SR: "+(sr/played));
	}
	
	private static void printAverageBowlerStats(Bowler bowler, List<BowlerStats> stats) {
		double played = 0, wickets = 0, rpo = 0;
		for (BowlerStats bwlStats : stats) {
			if (bwlStats.getBallsBowled()>0) {
				wickets += bwlStats.getWicketsTaken();
				rpo += bwlStats.getRPO();
				played++;
			}
		}
		System.out.println("Bowler: "+bowler.getName()+"\tPlayed: "+played
				+"\tAvg-Wicket: "+(wickets/played)+ "\tAvg-RPO: "+(rpo/played));
	}
	
	private static void testAggregateStats(int maxOvers, int targetScore, int numberMatchesToSimulate) {
		Map<Batsman, List<BatsmanStats>> batsman2Stats = new LinkedHashMap<>();
		Map<Bowler, List<BowlerStats>> bowler2Stats = new LinkedHashMap<>();
		for (int i=0; i<numberMatchesToSimulate; i++) {
			try {
				AIEngine engine = simulateInnings(maxOvers, targetScore, false);
				for (Map.Entry<Batsman, BatsmanStats> entry : engine.getBatsmanStats().entrySet()) {
					Batsman batsman = entry.getKey();
					List<BatsmanStats> stats = batsman2Stats.get(batsman);
					if (stats == null) {
						stats = new ArrayList<>();
						batsman2Stats.put(batsman, stats);
					}
					stats.add(entry.getValue());
				}
				for (Map.Entry<Bowler, BowlerStats> entry : engine.getBowlerStats().entrySet()) {
					Bowler bowler = entry.getKey();
					List<BowlerStats> stats = bowler2Stats.get(bowler);
					if (stats == null) {
						stats = new ArrayList<>();
						bowler2Stats.put(bowler, stats);
					}
					stats.add(entry.getValue());
				}
			} 
			catch (Exception e) {}
		}
		System.out.println("Printing Aggregate Stats\n\nBatsman:");
		for (Map.Entry<Batsman, List<BatsmanStats>> entry : batsman2Stats.entrySet()) {
			printAverageBatsmanStats(entry.getKey(), entry.getValue());
		}
		System.out.println("\nBowlers:");
		for (Map.Entry<Bowler, List<BowlerStats>> entry : bowler2Stats.entrySet()) {
			printAverageBowlerStats(entry.getKey(), entry.getValue());
		}
	}
	
	public static boolean playRealMatch(int numOvers, boolean displayGraphs) {
		int targetScore = numOvers * 9;
		int battingTeamIndex,pitch,climate;
		MatchInfo mi = Utils.createMatchInfo(numOvers, createIndianTeam(), createAustralianTeam());
		// toss to decide who bats first
		System.out.println("Select your team: 1. India 2. Australia");
		Scanner input = new Scanner(System.in);
		int team = input.nextInt();
		if (team == 1) {
			battingTeamIndex = 1;
		}
		else {
			battingTeamIndex = 2;
		}
		System.out.println("enter the pitch type: 1.Dusty 2.Hard 3.Flat 4.Grenn-Top");
		pitch = input.nextInt();
		//System.out.println("climatic conditions: 1. hot(Blue Sky) 2. humid 3. sunny");
		climate = 0;
		//int toss = Math.random()>0.5 ? 1 : 2;
		//batting team index == 1 --> Batting
		// batting team index == 2 --> bowling
		Team team1 = battingTeamIndex==1 ? mi.getTeam1() : mi.getTeam2();
		AIEngine ai1 = Utils.createAIEngine(mi, battingTeamIndex, true, targetScore,pitch,climate);
		ai1.playRealInnings(true);
		// get target for second innings
		targetScore = ai1.getMatchState().getRunsScored()+1;
		battingTeamIndex = battingTeamIndex==1 ? 2 : 1;
		Team team2 = battingTeamIndex==1 ? mi.getTeam1() : mi.getTeam2();
		AIEngine ai2 = Utils.createAIEngine(mi, battingTeamIndex, false, targetScore,pitch,climate);
		ai2.playRealInnings(true);
		MatchState finalState = ai2.getMatchState();
		
		Map<Team, Pair<ResultHistory, MatchState>> team2History = new LinkedHashMap<>();
		team2History.put(team1, new Pair<>(ai1.getGameHistory(), ai1.getMatchState()));
		team2History.put(team2, new Pair<>(ai2.getGameHistory(), ai2.getMatchState()));
		if (displayGraphs) {
			DrawScoringCharts.renderDataset(team2History);
		}
		
		return finalState.getRunsScored()>=targetScore;
	}
	
	private static void playMatches(int matches,int numOvers) {
		int chasingWon = 0;
		for (int i=0; i<matches; i++) {
			System.out.println("=== Starting New "+numOvers+" Over Match (#"+(i+1)+") ===");
			if (playRealMatch(numOvers, matches == 1)) {
				chasingWon++;
			}
		}
		System.out.println("Chasing team won: "+chasingWon+"\tTotal: "+matches);
	}
	
	public static void main(String[] args) throws Exception {
		/**
		 * Usage: [mode] [numOvers] [targetScore] [numMatches]
		 * 
		 * mode="sim" : simulate an innings with random decisions
		 * mode="aggregate" : run 10K simulations and display aggregate player statistics
		 * mode="play" : play an AI vs AI game
		 */
		if (args.length>0) {
			String mode = args[0];
			int numOvers = args.length>1 ? Integer.valueOf(args[1]) : 20; // default 20 overs
			int targetScore = args.length>2 ? Integer.valueOf(args[2]) : 200; // default 200 runs
			int numMatches = args.length>3 ? Integer.valueOf(args[3]) : 1;
			
			if (mode.equals("sim")) {
				simulateInnings(numOvers, targetScore, true);
			}
			else if (mode.equals("aggregate")) {
				testAggregateStats(numOvers, targetScore, 10000);
			}
			else if (mode.equals("play")){
				playMatches(numMatches, numOvers);
			}
		}
		else {
			// default: play 1 AI vs AI 20 over game
			playMatches(1, 20);
		}
	}
}
