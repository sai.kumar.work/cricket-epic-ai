package cricket.utils;

import cricket.aiengine.AIEngine;
import cricket.aiengine.BattingTeamAI;
import cricket.aiengine.BowlingTeamAI;
import cricket.aiengine.FieldingTeamAI;
import cricket.aiengine.WicketKeeperTeamAI;
import cricket.model.Batsman;
import cricket.model.Bowler;
import cricket.model.GameRules;
import cricket.model.MatchInfo;
import cricket.model.Team;
import cricket.model.Fielder;
import cricket.model.WicketKeeper;

public class Utils {

	public static MatchInfo createMatchInfo(int overs, Team team1, Team team2) {
		MatchInfo ret = new MatchInfo();
		ret.setTeam1(team1);
		ret.setTeam2(team2);
		GameRules rules = new GameRules();
		rules.setMaxOvers(overs);
		rules.setMaxWickets(ret.getTeam1().getBatsmen().size()-1);
		ret.setRules(rules);
		return ret;
	}
	
	public static AIEngine createAIEngine(
			MatchInfo matchInfo, 
			int battingTeamIndex, 
			boolean settingTarget,
			int targetScore, int pitch, int climate) {
		AIEngine ret = new AIEngine();
		// which gonna bat now
		Team battingTeam = battingTeamIndex==1 ? matchInfo.getTeam1() : matchInfo.getTeam2();
		// which gonna field now
		Team fieldingTeam = battingTeamIndex==1 ? matchInfo.getTeam1() : matchInfo.getTeam2();
		// who is wicket keeper now
		Team wicketkepperteam = battingTeamIndex==1 ? matchInfo.getTeam1() : matchInfo.getTeam2();
		// who gonna bowl now
		Team bowlingTeam = battingTeamIndex==1 ? matchInfo.getTeam2() : matchInfo.getTeam1();
		BattingTeamAI battingAI = new BattingTeamAI(settingTarget);
		for (Batsman batsman : battingTeam.getBatsmen()) {
			battingAI.addBatsman(batsman);
		}
		FieldingTeamAI fieldingAI = new FieldingTeamAI();
		for (Fielder fielder : fieldingTeam.getFielders()) {
			fieldingAI.addFielder(fielder);
		}
		WicketKeeperTeamAI wicketkeeperAI = new WicketKeeperTeamAI();
		for (WicketKeeper wicketkepper : wicketkepperteam.getWicketKeeper()) {
			wicketkeeperAI.addWicketKeeper(wicketkepper);
		}
		BowlingTeamAI bowlingAI = new BowlingTeamAI();
		final int overs = matchInfo.getRules().getMaxOvers();
		final int totalNumBowlers = bowlingTeam.getBowlers().size();
		final int pit = pitch;
		final int clm = climate;
		final int limitOvers = overs % totalNumBowlers == 0 ? (overs/totalNumBowlers) : (overs/totalNumBowlers)+1;
		for (Bowler bowler : bowlingTeam.getBowlers()) {
			bowlingAI.addBowler(bowler, Math.max(1, limitOvers));	
		}
		ret.initInnings(battingAI, bowlingAI,fieldingAI,wicketkeeperAI, matchInfo.getRules(), targetScore,pit,clm);
		return ret;
	}
}
