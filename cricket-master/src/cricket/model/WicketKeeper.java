package cricket.model;

public class WicketKeeper extends Player {


	private int precision;
	
	public WicketKeeper(String name, int precision) {
		this.name = name;

		this.precision = precision;
	}
	

	
	public int getPre() {
		return this.precision;
	}

	public WicketKeeper copy() {
		WicketKeeper ret = new WicketKeeper(name,  precision);
		return ret;
	}
}
