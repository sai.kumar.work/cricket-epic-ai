package cricket.model;

public class Fielder extends Player {

	private double speed, role;
	private int accuracy;
	
	public Fielder(String name, double speed, double role, int accuracy) {
		this.name = name;
		this.speed = speed;
		this.role = role;
		this.accuracy = accuracy;
	}
	
	public double getSpeed() {
		return this.speed;
	}
	
	public double getRole() {
		return this.role;
	}
	public int getAcc() {
		return this.accuracy;
	}
	public Fielder copy() {
		Fielder ret = new Fielder(name, speed,role,accuracy);
		return ret;
	}
}
