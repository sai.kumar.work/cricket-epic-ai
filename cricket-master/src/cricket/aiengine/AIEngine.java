package cricket.aiengine;
import java.util.Random;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cricket.model.BallDelivery;
import cricket.model.Batsman;
import cricket.model.BatsmanIntent;
import cricket.model.BatsmanStats;
import cricket.model.Bowler;
import cricket.model.BowlerStats;
import cricket.model.GameEvent;
import cricket.model.GameRules;
import cricket.model.MatchState;
import cricket.model.ResultHistory;
import cricket.model.ShotOutcome;
import cricket.model.WicketType;
import cricket.model.Fielder;
import cricket.model.WicketKeeper;
/**
 * The core AI engine for the game. 
 * Key method: simulateInnings(..)
 * Parameters in this class have been tuned over 10K simulations checking that 
 * aggregate batsman and bowler statistics reflect their attributes.
 */
public class AIEngine {

	private BattingTeamAI battingAI;
	private BowlingTeamAI bowlingAI;
	public FieldingTeamAI fieldingAI;
	private ResultHistory gameHistory;
	public WicketKeeperTeamAI wicketkeeperAI;
	private MatchState matchState;
	private final double minMetric = 0.5;
	private final double maxMetric = 9.5;
	boolean number;
	int pitch, climate;
	private final double mediumBallMovement = 5;
	private final double highBallSpeed = 8;
	private final double changeBatsmanExecutionPreferredAverse = 0.2; 
	private boolean varyExecutionBasedOnHistory = true;
	
	private boolean printPlayByPlay = false;
	
	public void initInnings(BattingTeamAI battingAI, BowlingTeamAI bowlingAI,FieldingTeamAI fieldingAI,WicketKeeperTeamAI wicketkeeperAI, GameRules rules, int targetScore, int pitch, int climate) {
		this.battingAI = battingAI;
		this.bowlingAI = bowlingAI;
		this.fieldingAI = fieldingAI;
		this.wicketkeeperAI = wicketkeeperAI;
		this.gameHistory = new ResultHistory();
		this.pitch = pitch;
		this.climate = climate;
		this.matchState = new MatchState(rules.getMaxOvers(), 0, rules.getMaxWickets(), battingAI.isSettingTarget(), number);
		Batsman striker = battingAI.getOpenerStriker();
		Batsman nonStriker = battingAI.getOpenerNonStriker();
		this.matchState.setCurrentStriker(striker);
		this.matchState.setCurrentNonStriker(nonStriker);
		this.matchState.setTargetRuns(targetScore);	
	}
	// vary based on the history
	private double varyBowlerExecutionOnHistory(Bowler bowler) {
		double execution = bowler.getExecution();
		BowlerStats stats = bowlingAI.getBowlerStats(bowler);
		double rpo = stats.getRPO();
		int wickets = stats.getWicketsTaken();
		double execChange = stats.getBallsBowled() < 10 ? 0 : Math.max(-1, Math.min(0.5, (10 - rpo) * 0.1));
		execChange += wickets < 1 ? 0 : (wickets-1) * 0.2;

		return Math.max(minMetric, Math.min(maxMetric, execution + execChange));
	}
	
	private double varyBatsmanExecutionOnHistory(Batsman batsman) {
		double execution = batsman.getExecution();
		BatsmanStats stats = battingAI.getBatsmanStats(batsman);
		int runsScored = stats.getRunsScored();
		int boundaries = stats.getFoursHit() + stats.getSixesHit();
		double execChange = stats.getBallsFaced() < 10 ? 0 : Math.max(-0.5, Math.min(0.5, (runsScored - 15) *  0.01));
		execChange += Math.min(1, boundaries * 0.1);
		//System.out.println("outcome: "+Math.min(maxMetric, execution + execChange));
		return Math.max(minMetric, Math.min(maxMetric, execution + execChange));
	}
	
	private double VaryWicketkeeperExecution(WicketKeeper wicketkeeper) {
		int agility = wicketkeeper.getPre();
		double agilitydiff = matchState.getMaxOvers() - matchState.getOversCompleted() > 10 ? getwick10() : getwick20();
		double precision = agility/10 + agilitydiff;
		return precision;
	}
	
	public BallDelivery computeFinalBallDelivery(
			BallDelivery intendedBallDelivery) {
		Bowler bowler = intendedBallDelivery.getBowler();
		double pitchExecution = 0;
		// change execution considering bowler history
		double execution = varyExecutionBasedOnHistory ? varyBowlerExecutionOnHistory(bowler)
								: bowler.getExecution();
		//System.out.println(bowler.getType());
		// for pitch-zone need to vary across 2 dimensions
		double finalPitchZone = varyBasedOnExecution(intendedBallDelivery.getPitchZone(),
				execution, minMetric, maxMetric, 0.5);
		double diffPZ = finalPitchZone - intendedBallDelivery.getPitchZone();
		double varXDim = (execution/10) * diffPZ;
		double varYDim = diffPZ - varXDim;
		// the pitch selection values will have the effect on the below parameters.
		if (pitch == 1) {
			if (bowler.getType() == 2) {
				pitchExecution = execution/10;
			}
		}
		else if (pitch == 4) {
			if (bowler.getType() == 1) {
				pitchExecution = execution/10;
			}
		}
		else {
			pitchExecution = 0;
		}
		finalPitchZone = intendedBallDelivery.getPitchZone() + varXDim + (3*varYDim) + pitchExecution;
		//System.out.println("the pitch zone is: "+finalPitchZone);
		// both movement and speed vary according to ability and execution
		double finalMovement = varyBasedOnAbilityExecution(intendedBallDelivery.getMovement(), 
				bowler.getMovement(), execution, minMetric, maxMetric);
		double finalSpeed = varyBasedOnAbilityExecution(intendedBallDelivery.getSpeed(), 
				bowler.getSpeed(), execution, minMetric, maxMetric);
		return new BallDelivery(finalSpeed, finalMovement, finalPitchZone, bowler);
	}
	
	
	private double varyBasedOnAbilityExecution(double metric, double ability, double execution, double min, double max) {
		//metric = Math.min(metric, ability);
		double dirnBias = 0.5;
		if (metric > ability) {
			metric = (metric + ability) / 2;
			execution = (ability/metric) * execution;
			dirnBias = 0.9;
		}
		return varyBasedOnExecution(metric, execution, min, max, dirnBias);
	}
	
	private double varyBasedOnExecution(double metric, double execution, double min, double max, double dirnBias) {
		double variance = ((10 - execution) / 10) * metric;
		double change = Math.random() - dirnBias;
		return Math.max(min, Math.min(metric + change * variance, max));
	}
	
	
	// random generator which is this algorithm based on the
	private double getRandom0To10() {
		return Math.random()*10;
	}

	private double getRandom0To1() {
		return Math.random();
	}
	
	//wicketkeeper concentration
	
	private double getwick10() {
		return Math.random()*3;
	}
	private double getwick20() {
		return Math.random();
	}
	
	// for moderate runners
	private double getMod() {
		return Math.random()*0.25;
	}
	// for very fast runners
	private double getFast() {
		return Math.random()*0.5;
	}
	
	//fast
	private double Faster() {
		return Math.random()*0.75;
	}
	// random fielder generator
	public int fieldergenerator() {
		Random rand = new Random();
		int max = 9;
		int min = 0;
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}
	
	//the functions defining shot outcome
	//0 --> wicket potential
	//1 --> run potential
	//2 --> lofted potential
	// this funtion will take the pitch as the input and respective variables like run potential, wicket potential, lofted potential.
	public double[] pitchconditions(int pitch) {
		double[] outcome = new double[3];
		if (pitch == 2  || pitch == 3 ) {
			outcome[0] = 0;
			outcome[1] = 0.1;
			outcome[2] = 0;
		}
		return outcome;
	}
	
	
	// will take the batsmen speed as the input and will gives the out come
	public double[] batsmen(Batsman batsman) {
		int speed = batsman.getSpeed();
		boolean shotExecutedWell = getRandom0To10() < batsman.getExecution();
		boolean madeContact = getRandom0To10() < batsman.getContact();
		boolean hasPower = getRandom0To10() < batsman.getPower();
		double[] outcome = new double[3];
		if (speed == 9) {
			outcome[0] = 0;
			outcome[1] = Faster();
			outcome[2] = 0;
		}
		if (speed == 8) {
			outcome[0] = 0;
			outcome[1] = getFast();
			outcome[2] = 0;
		}
		if (speed == 7) {
			outcome[0] = 0;
			outcome[1] = getMod();
			outcome[2] = 0;
		}
		// for batsmen
		if (shotExecutedWell) {
			outcome[1] += Math.random();
			outcome[0] -= Math.random()*2;
		}
		else {
			outcome[1] -= Math.random();
			outcome[0] += Math.random();
		}
		if (madeContact) {
			outcome[1] += Math.random();
			//wicketPotential -= Math.random()*2;
			if (hasPower) {
				outcome[1] += Math.random();
			}
		}
		else {
			outcome[1] -= Math.random();
			//wicketPotential += Math.random();
		}
		//match situation and the target runs
		if(matchState.getRunsScored() < matchState.getTargetRuns() && matchState.getMaxOvers() - matchState.getOversCompleted() <10) {
			if(batsman.getExecution() >=8) {
				outcome[1] += Math.random()*2;
			}
			else {
				outcome[1] += Math.random();
			}
			
		}
		
		return outcome;
	}

	
	// fielder effect and the outcome
	public double[] fielderoutcome(double d,int accuracy, int maxovers) {
		boolean powerplay;
		if (maxovers == 20) {
			powerplay = 6< matchState.getOversCompleted();
		}
		else if (maxovers == 50) {
			powerplay = 10< matchState.getOversCompleted();
		}
		else {
			powerplay = false;
		}
		double[] outcome = new double[3];
		if(d == 9) {
			outcome[1] -= 0.25;
		}
		
		else if(d == 8) {
			outcome[1] -= 0.2;
		}
		else if(d == 7) {
			outcome[1] -= 0.15;
		}
		
		if(powerplay = true) {
			if (accuracy >= 8) {
				outcome[1]-= 0.2;
			}
			if (accuracy < 8) {
				outcome[1] -= 0.1;
			}
		}
		if(powerplay == false) {
			if (accuracy >= 8) {
				outcome[1]-= 0.35;
			}
			if (accuracy < 8) {
				outcome[1] -= 0.25;
			}
		}
		return outcome;
		
	}
	
	
	// match conditions and its outcome
	public double[] matchconditions() {
		double[] outcome = new double[3];
		if (matchState.getMaxOvers() == 20) {
			if (matchState.getOversCompleted() < 6) {
				outcome[1] += Math.random();
				outcome[0] -= Math.random();
			}
			else if (matchState.getOversCompleted() > 14) {
				outcome[1] += Math.random()*2;
				outcome[0] -= Math.random();
			}
		}
		if (matchState.getMaxOvers() == 20) {
			if(matchState.getOversCompleted() > 6 && matchState.getOversCompleted() < 15) {
				outcome[1] -= Math.random();
				//loftedPotential += Math.random()*2;
			}
		}
		if (matchState.getMaxOvers() == 50) {
			if (matchState.getOversCompleted() < 10) {
				outcome[1] += Math.random();
				outcome[0] -= Math.random();
			}
			else if (matchState.getOversCompleted() > 45) {
				outcome[1] += Math.random()*2;
				outcome[0] -= Math.random();
			}
		}
		if (matchState.getMaxOvers() == 20) {
			if(matchState.getOversCompleted() > 10 && matchState.getOversCompleted() < 45) {
				outcome[1] -= Math.random();
				//loftedPotential += Math.random()*2;
			}
		}
		if (matchState.getWicketsFallen() >4 && matchState.getOversCompleted() <15) {
			outcome[0] -= 0.1;
			outcome[1] += 0.1;
		}
		return outcome;
	}
	
	
	//batsman weakness
	public double[] weakness(Batsman batsman, BallDelivery finalBallDelivery) {
		double[] outcome = new double[3];
		//batsman weakness 
		
		if (batsman.getWeakness() == 1 && finalBallDelivery.getBowler().getType() == 1) {
			outcome[1] -= Math.random();
			outcome[2] += Math.random();
		}
		if (batsman.getWeakness() == 2 && finalBallDelivery.getBowler().getType() == 2) {
			outcome[1] -= Math.random();
			outcome[2] += Math.random();
		}
		if (batsman.getWeakness() == 3 && finalBallDelivery.getBowler().getType() == 2) {
			outcome[1] -= Math.random()*1.5;
		}
		if (batsman.getWeakness() == 4 && finalBallDelivery.getBowler().getType() == 1) {
			outcome[1] -= Math.random()*1.5;
		}
		if (batsman.getWeakness() == 5 && finalBallDelivery.getBowler().getType() == 1) {
			outcome[1] -= Math.random();
			outcome[0]-= Math.random();
		}
		if (batsman.getWeakness() == 6 && finalBallDelivery.getBowler().getType() == 1) {
			outcome[1] -= Math.random()*2;
		}	
		return outcome;
	}
	
	
	//signature shot function
	public double[] strength(BallDelivery finalBallDelivery, Batsman batsman) {
		double[] outcome = new double[3];
		//signature shots
		boolean cutshot = 1 == batsman.getSignature();
		boolean pullshot = 2 == batsman.getSignature();
		boolean coverdrive = 3 == batsman.getSignature();
		boolean helicopter = 4 == batsman.getSignature();
		boolean sweep = 5 == batsman.getSignature();
		boolean insideout = 6 == batsman.getSignature();
		//signature shots
		if (cutshot && finalBallDelivery.isShort()) {
			//loftedPotential += Math.random()*2;
			outcome[1] += Math.random();
		}
		
		if (pullshot && finalBallDelivery.isGoodLength()) {
			//loftedPotential += Math.random()*2;
			outcome[1] += Math.random();
		}
		
		if (coverdrive && finalBallDelivery.isGoodLength()) {
			//loftedPotential += Math.random()*2;
			outcome[1] += Math.random();
		}
		
		if (helicopter && finalBallDelivery.isGoodLength() || finalBallDelivery.isYorker()) {
			//loftedPotential += Math.random()*2;
			outcome[1] += Math.random();
		}
		
		if (sweep && finalBallDelivery.getBowler().getSpeed() < 6.5) {
			//loftedPotential += Math.random()*2;
			outcome[1] += Math.random();
		}
		
		if (insideout && finalBallDelivery.isShort()) {
			//loftedPotential += Math.random()*2;
			outcome[1] += Math.random();
		}	
		return outcome;
	}
	
	
	//bowler effect function
	public double[] balldelivary(Batsman batsman, BallDelivery finalBallDelivery) {
		double[] outcome = new double[3];
		double execution = varyExecutionBasedOnHistory ? varyBatsmanExecutionOnHistory(batsman) :
			batsman.getExecution();
		double execDiff = finalBallDelivery.getBowler().getExecution() - execution;
		boolean bowlerBetter = getRandom0To10() < execDiff;
		boolean bowledWell = getRandom0To10() < finalBallDelivery.getBowler().getExecution();
		if (finalBallDelivery.isYorker() || finalBallDelivery.isGoodLength()) {
			outcome[1] -= Math.random();
			outcome[0] += Math.random();
			outcome[2] -= Math.random()*2;
		}
		else if (finalBallDelivery.isOutSwinger() || finalBallDelivery.isInSwinger()){
			outcome[0] += Math.random()*2;
			outcome[1] -= Math.random();
		}
		// bowling attribute spinners
		else if ( finalBallDelivery.isCarromBall() || finalBallDelivery.isGoogly()) {
			outcome[0] += Math.random();
			outcome[1] -= Math.random();
		}
		
		else if(finalBallDelivery.isDoosra() || finalBallDelivery.isBackspin()) {
			outcome[0] -= Math.random();
			outcome[1] -= Math.random();
		}
		// bowling variables for bowlers 
		if (finalBallDelivery.getMovement()>mediumBallMovement
				&& finalBallDelivery.getSpeed()>highBallSpeed) {
			outcome[0] += Math.random();
		}
		else {
			outcome[1] += Math.random();
		}
		
		if (finalBallDelivery.isHalfVolley() || finalBallDelivery.isFullToss()) {
			outcome[1] += Math.random()*2;
			outcome[0] -= Math.random();
		}
		
		if (bowlerBetter) {
			outcome[1] -= Math.random();
			outcome[0] += Math.random();
		}
		else {
			outcome[1] += Math.random();
			outcome[0] -= Math.random(); 
		}
		
		if (bowledWell) {
			outcome[1] -= Math.random();
			outcome[0] += Math.random();
		}
		else {
			outcome[1] += Math.random();
			//wicketPotential -= Math.random(); 
		}
		
		return outcome;
	}
	
	
	//wicket keeper effect
	public double[] wicketkeeperoutcome(Batsman batsman, BallDelivery finalBallDelivery) {
		double[] outcome = new double[3];
		WicketKeeper wicketkeeper = wicketkeeperAI.getWicketKeeper().get(0);
		double precision = VaryWicketkeeperExecution(wicketkeeper);
		if(batsman.getExecution() >= 8) {
			if (finalBallDelivery.getPitchZone() >= 1 && finalBallDelivery.getPitchZone() <= 1.9 || finalBallDelivery.getPitchZone() >= 3 && finalBallDelivery.getPitchZone() <= 3.9) {
				outcome[0] += 0.1;
				outcome[2] += 0.1;
			}
		}
		
		else if(batsman.getExecution() < 7) {
			if (finalBallDelivery.getPitchZone() >= 1 && finalBallDelivery.getPitchZone() <= 1.9 || finalBallDelivery.getPitchZone() >= 3 && finalBallDelivery.getPitchZone() <= 3.9) {
				outcome[0] += 0.2;
				outcome[2] += 0.2;

			}
		}
		if (matchState.getMaxOvers() - matchState.getOversCompleted() < 5) {
			if (finalBallDelivery.getPitchZone() >= 1 && finalBallDelivery.getPitchZone() <= 1.9 || finalBallDelivery.getPitchZone() >= 3 && finalBallDelivery.getPitchZone() <= 3.9) {
				outcome[0] += 0.3;
				outcome[2] += 0.4;

			}
		}


		return outcome;
	}
	
	
	/// running all the functions
	public ShotOutcome computeShotOutcome(
			BallDelivery finalBallDelivery, 
			Batsman batsman, 
			BatsmanIntent intent,
			Batsman nonStriker) {
		
		ShotOutcome result = new ShotOutcome();
		result.setStriker(batsman);
		result.setNonStriker(nonStriker);
		// fielder selection
		Fielder fielder = fieldingAI.getFielders().get(fieldergenerator());
		result.setFielder(fielder);
		//Bowler bowler = finalBallDelivery.getBowler();
		// change batsman execution based on preferred/averse ball delivery 
		// change execution considering batsman history
		double execution = varyExecutionBasedOnHistory ? varyBatsmanExecutionOnHistory(batsman) :
							batsman.getExecution();
		
		if (batsman.isPreferredBall(finalBallDelivery)) {
			execution = Math.min(maxMetric, (1+changeBatsmanExecutionPreferredAverse)*execution);
		}
		if (batsman.isAverseToBall(finalBallDelivery)) {
			execution = Math.max(minMetric, (1-changeBatsmanExecutionPreferredAverse)*execution);
		}
		
		// recompute aggression considering latitude and ball delivery
		double aggression = intent.getAggression();
		if (finalBallDelivery.isGoodLength() || finalBallDelivery.isYorker() 
				|| (finalBallDelivery.isShort() && batsman.isAverseToBall(finalBallDelivery))) {
			aggression -= intent.getLatitude();
		}
		else {
			aggression = Math.min(maxMetric, aggression + Math.random()*intent.getLatitude());
		}
		result.setFinalAggression(aggression);
		
		// determine key conditions affecting outcome
		boolean shotExecutedWell = getRandom0To10() < batsman.getExecution();

		// variables initiliation
		double wicketPotential = 0, loftedPotential = 0, runPotential = 0;
		
		// this will all depends on the functions
		// bowling attributes 
		double[] bow = balldelivary(batsman,finalBallDelivery);
		wicketPotential += bow[0];
		runPotential += bow[1];
		loftedPotential += bow[2];
		
		// pitch outcome 
		double[] p = pitchconditions(pitch);
		wicketPotential += p[0];
		runPotential += p[1];
		loftedPotential += p[2];
		
		
		// speed outcome 
		double[] s = batsmen(batsman);
		wicketPotential += s[0];
		runPotential += s[1];
		loftedPotential += s[2];
		
		
		// fielder outcome
		double[] f = fielderoutcome(fielder.getSpeed(),fielder.getAcc(),matchState.getMaxOvers());
		wicketPotential += f[0];
		runPotential += f[1];
		loftedPotential += f[2];	
	
		
		// match overs effect on the outcome
		double[] o = matchconditions();
		wicketPotential += o[0];
		runPotential += o[1];
		loftedPotential += o[2];	
		
		
		// signature shot outcome	
		double[] sig = strength(finalBallDelivery,batsman);
		wicketPotential += sig[0];
		runPotential += sig[1];
		loftedPotential += sig[2];	
		
		
		//batsman weakness 
		double[] weakness = weakness(batsman,finalBallDelivery);
		wicketPotential += weakness[0];
		runPotential += weakness[1];
		loftedPotential += weakness[2];	
	
		
		//wicket-keeper outcome
		double[] wk = wicketkeeperoutcome(batsman,finalBallDelivery);
		wicketPotential += wk[0];
		runPotential += wk[1];
		loftedPotential += wk[2];		
		
		
		double runMultiplier =  3+aggression / 2;
		loftedPotential += aggression / 15;
		wicketPotential *= aggression / 2.2;
		
		boolean runs = getRandom0To1() < runPotential;
		if (runs) {
			runPotential = runMultiplier;
		}
		boolean wicketFell = getRandom0To10() < wicketPotential;
		boolean loftedShot = getRandom0To1() < loftedPotential;
		int runsScored = new Random().nextInt(Math.max(1, (int) runPotential));
		
		if (runsScored == 5) {
			runsScored = 4;
		}
		if (runsScored > 6) {
			runsScored = 6;
		}
		if (runsScored == 3) {
			Random random = new Random();
			runsScored = random.nextBoolean() ? 2 :3;
		}
		if (intent.isChangeStrike() && shotExecutedWell) {
			if (runsScored >= 2) 
				runsScored = 3;
			else if (runsScored > 0)
				runsScored = 1;
			if (runsScored == 3) {
				Random random = new Random();
				runsScored = random.nextBoolean() ? 2:3;
			}
		}
		result.setLoftedShot(loftedShot);
		if (wicketFell) {
			result.setWicketFell(true);
			WicketType wt = loftedShot ? WicketType.caught : 
							runsScored < 2 ? WicketType.lbw 
									: Math.random()<0.2 ? WicketType.stumped :
										Math.random()<0.5 ? WicketType.bowled : WicketType.caught;
			result.setFielder(fielder);
			result.setWicketType(wt);
			result.setRunsScored(0);
		}
		else {
			result.setWicketFell(false);
			result.setRunsScored(runsScored);
		}
		//System.out.println("bowling action: "+bowler.getType());
		return result;
	}
	
	public void simulateInnings() {
		AIDecisionMode[] decisionMade = new AIDecisionMode[4];
		for (int i=0; i<4; i++) {
			decisionMade[i] = AIDecisionMode.sample;
		}
		simulateInnings(decisionMade);
	}
	
	public void simulateInnings(AIDecisionMode[] decisionVector) {
		//System.out.println(access_fielders);
		long time = System.currentTimeMillis();
		while (!matchState.isGameOver()) {
			// determine bowler
			Bowler currentBowler = null;
			if (decisionVector[0] == AIDecisionMode.sample) {
				currentBowler = bowlingAI.sampleNextBowler(matchState.getCurrentBowler());
				if (currentBowler == null) {
					matchState.setInvalid(true);
					matchState.setInvalidStateReason("No more bowlers available");
					gameHistory.addToHistory(
							null, null, null, null, matchState.copy());
					break;
				}
			}
			else if (decisionVector[0] == AIDecisionMode.retrieve) {
				currentBowler = bowlingAI.getNextBowler();
				decisionVector[0] = AIDecisionMode.sample;
			}
			else if (decisionVector[0] == AIDecisionMode.decide) {
				currentBowler = (Bowler) MonteCarloSampler.decideUsingMCSampling(this, 0);
				bowlingAI.setNextBowler(currentBowler);
				if (printPlayByPlay) {
					System.out.println("Next Bowler: "+currentBowler);
				}
			}
			matchState.setCurrentBowler(currentBowler);
			
			// simulate an over
			boolean overOrMatchDone = false;
			int ballsInOverBowled = matchState.getBallsInOver();
			while (!overOrMatchDone) {
				
				// determine bowler intent
				BallDelivery intendedBallDelivery = null;
				if (decisionVector[1] == AIDecisionMode.sample) {
					intendedBallDelivery = bowlingAI.sampleIntendedBallDelivery(currentBowler);
				}
				else if (decisionVector[1] == AIDecisionMode.retrieve) {
					intendedBallDelivery = bowlingAI.getNextIntendedBallDelivery();
					decisionVector[1] = AIDecisionMode.sample;
				}
				else if (decisionVector[1] == AIDecisionMode.decide) {
					intendedBallDelivery = (BallDelivery) MonteCarloSampler.decideUsingMCSampling(this, 1);
					bowlingAI.setNextIntendedBallDelivery(intendedBallDelivery);
				}
				else if (decisionVector[1] == AIDecisionMode.shot_sim) {
					intendedBallDelivery = BallDelivery.getGoodLength(currentBowler);
				}
				
				// determine final delivery
				BallDelivery finalBallDelivery = computeFinalBallDelivery(intendedBallDelivery);
				
				// determine batsman intent
				BatsmanIntent intent = null;
				if (decisionVector[2] == AIDecisionMode.sample) {
					intent = battingAI.sampleNextIntent();
				}
				else if (decisionVector[2] == AIDecisionMode.retrieve){
					intent = battingAI.getNextBattingIntent();
					//decisionVector[2] = AIDecision.sample;
				}
				else if (decisionVector[2] == AIDecisionMode.decide) {
					intent = (BatsmanIntent) MonteCarloSampler.decideUsingMCSampling(this, 2);
					battingAI.setNextBattingIntent(intent);
				}
				
				// determine outcome of delivery
				ShotOutcome outcome = computeShotOutcome(finalBallDelivery,
						matchState.getCurrentStriker(), intent, matchState.getCurrentNonStriker());
				// add outcome to batsman history
				battingAI.addToHistory(matchState.getCurrentStriker(), 
						intendedBallDelivery, finalBallDelivery, intent, outcome, matchState.copy());
				
				// process outcome
				if (outcome.isWicketFell()) {
					Batsman outBatsman = outcome.getWicketType()==WicketType.run_out_non_striker ? matchState.getCurrentNonStriker()
							: matchState.getCurrentStriker();
					Batsman nextBatsman = null;
					// figure out next batsman if wicket fell
					if (decisionVector[3] == AIDecisionMode.sample) {
						nextBatsman = battingAI.sampleNextBatsman();
					} 
					else if (decisionVector[3] == AIDecisionMode.retrieve) {
						nextBatsman = battingAI.getNextBatsman();
						decisionVector[3] = AIDecisionMode.sample;
					}
					else if (decisionVector[3] == AIDecisionMode.decide && battingAI.hasRemainingBatsmen()) {
						nextBatsman = (Batsman) MonteCarloSampler.decideUsingMCSampling(this, 3);
						battingAI.updateRemainingBatsman(nextBatsman);
						battingAI.setNextBatsman(nextBatsman);
					}
					
					if (outBatsman == matchState.getCurrentStriker()) {
						matchState.setCurrentStriker(nextBatsman);
					}
					else {
						matchState.setCurrentNonStriker(nextBatsman);
					}
				}
				
				// add to game history
				GameEvent event = new GameEvent(intendedBallDelivery.copy(), 
						finalBallDelivery.copy(), 
						intent.copy(), 
						outcome, matchState.copy());
				
				gameHistory.addToHistory(event);
			
				if (printPlayByPlay) {
					System.out.println(event.prettyPrint(battingAI, bowlingAI)+"\n");
				}
				
				// increment balls in over
				ballsInOverBowled++;
				matchState.setBallsInOver(ballsInOverBowled);
			
				// update match state and check if over or match done
				overOrMatchDone = matchState.updateMatchState(outcome);	
			}
			// this updates bowler history
			bowlingAI.updateOver(currentBowler, gameHistory, ballsInOverBowled);
		}
		//System.out.println("Sim Time: "+(System.currentTimeMillis()-time)+" ms");
	}
	
	public void printInningsDetail() {
		StringBuffer ret = new StringBuffer();
		for (GameEvent event : gameHistory.getGameEvents()) {
			ret.append(event.prettyPrint(battingAI, bowlingAI)+"\n\n");
		}
		System.out.println(ret);
		System.out.println("Final "+matchState);
	}
	
	public void printInningsSummary() {
		StringBuffer ret = new StringBuffer();
		ret.append("Batting Scorecard\n");
		ret.append("Batsman\t\tRuns\t4s\t6s\tBalls\tSR\n");
		for (Batsman batsman : battingAI.getBattingOrderChosen()) {
			BatsmanStats stats = battingAI.getBatsmanStats(batsman);
			String notOut = !stats.gotOut() ? "*" : "";
			ret.append(batsman.toString()+notOut+"\t"+stats.toString()+"\n");
		}
		ret.append("\nBowling Scorecard\n");
		ret.append("Bowler\t\tOvers\tRuns\tWickets\tRPO\n");
		for (Map.Entry<Bowler, BowlerStats> entry : bowlingAI.getBowlerStats().entrySet()) {
			ret.append(entry.getKey().toString()+"\t"+entry.getValue().toString()+"\n");
		}
		System.out.println(ret);
	}
	
	public Map<Batsman, BatsmanStats> getBatsmanStats() {
		return battingAI.getBatsmanStats();
	}
	
	public Map<Bowler, BowlerStats> getBowlerStats() {
		return bowlingAI.getBowlerStats();
	}
	
	public BattingTeamAI getBattingAI() {
		return battingAI;
	}
	
	public BowlingTeamAI getBowlingAI() {
		return bowlingAI;
	}
	
	public MatchState getMatchState() {
		return matchState;
	}
	
	public void setPrintPlayByPlay(boolean printPlays) {
		this.printPlayByPlay = printPlays;
	}
	
	public AIEngine copy() {
		AIEngine ret = new AIEngine();
		ret.battingAI = battingAI.copy();
		ret.fieldingAI = fieldingAI.copy();
		ret.wicketkeeperAI = wicketkeeperAI.copy();
		ret.bowlingAI = bowlingAI.copy();
		ret.pitch = pitch;
		ret.gameHistory = gameHistory.copy();
		ret.matchState = matchState.copy();
		return ret;
	}
	
	public void playRealInnings(boolean logResults) {
		setPrintPlayByPlay(logResults);
		AIDecisionMode[] decisionVector = new AIDecisionMode[4];
		for (int i=0; i<4; i++) {
			decisionVector[i] = AIDecisionMode.decide;
		}
		simulateInnings(decisionVector);
		if (logResults) {
			System.out.println(getMatchState().toString());
			printInningsSummary();
		}
	}

	public ResultHistory getGameHistory() {
		return gameHistory;
	}
	
	
}
