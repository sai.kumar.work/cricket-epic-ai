package cricket.aiengine;

import java.util.ArrayList;
import java.util.List;

import cricket.model.WicketKeeper;


public class WicketKeeperTeamAI {

	public List<WicketKeeper> wicketkeepers = new ArrayList<>();
	
	
	
	public void addWicketKeeper(WicketKeeper b) {
		this.wicketkeepers.add(b);

	}

	
	public List<WicketKeeper> getWicketKeeper() {
		return this.wicketkeepers;
	}
	
	
	public WicketKeeperTeamAI copy() {
		WicketKeeperTeamAI ret = new WicketKeeperTeamAI();
		for (WicketKeeper wicketkeeper : wicketkeepers) {
			ret.wicketkeepers.add(wicketkeeper.copy());
		}
		return ret;
	}

	
	
	
}
