package cricket.aiengine;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cricket.model.Batsman;
import cricket.model.Fielder;
import cricket.model.ResultHistory;

public class FieldingTeamAI {

	public List<Fielder> fielders = new ArrayList<>();
	public void addFielder(Fielder b) {
		this.fielders.add(b);

	}

	public List<Fielder> getFielders() {
		return this.fielders;
	}
	
	public FieldingTeamAI copy() {
		FieldingTeamAI ret = new FieldingTeamAI();
		for (Fielder fielder : fielders) {
			ret.fielders.add(fielder.copy());
		}
		return ret;
	}

	
	
	
}
